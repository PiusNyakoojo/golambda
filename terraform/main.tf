// TO-DO: Refactor resources into a module, and then make module(s)
// call in main.tf

// compile the code
resource "null_resource" "compile" {
  triggers = {
    build_number = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = "GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ../target/golambdabin -ldflags '-w' ../src/golambda.go"
  }
}

resource "aws_lambda_function" "dev-golambda" {
  function_name    = "dev-golambda"
  handler          = "golambdabin"
  runtime          = "go1.x"
  role             = "arn:aws:iam::814222117596:role/lambda-role"
  filename         = data.archive_file.zip.output_path
  source_code_hash = data.archive_file.zip.output_base64sha256
  memory_size      = 128
  timeout          = 10
}

resource "aws_lambda_permission" "allow_api" {
  statement_id  = "AllowAPIGatewayInvokation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.dev-golambda.function_name
  principal     = "apigateway.amazonaws.com"
}

############# API GATEWAY ############

resource "aws_api_gateway_rest_api" "dev-golambda" {
  name = "dev-golambda"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "person" {
  rest_api_id = aws_api_gateway_rest_api.dev-golambda.id
  parent_id   = aws_api_gateway_rest_api.dev-golambda.root_resource_id
  path_part   = "person"
}

// POST
resource "aws_api_gateway_method" "post" {
  rest_api_id      = aws_api_gateway_rest_api.dev-golambda.id
  resource_id      = aws_api_gateway_resource.person.id
  http_method      = "POST"
  authorization    = "NONE"
  api_key_required = false
}


resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.dev-golambda.id
  resource_id             = aws_api_gateway_resource.person.id
  http_method             = aws_api_gateway_method.post.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.dev-golambda.invoke_arn
  credentials = aws_lambda_function.dev-golambda.role
}

// GET
resource "aws_api_gateway_method" "get" {
  rest_api_id      = aws_api_gateway_rest_api.dev-golambda.id
  resource_id      = aws_api_gateway_resource.person.id
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = false
}

resource "aws_api_gateway_integration" "integration-get" {
  rest_api_id             = aws_api_gateway_rest_api.dev-golambda.id
  resource_id             = aws_api_gateway_resource.person.id
  http_method             = aws_api_gateway_method.get.http_method
  integration_http_method = "GET"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.dev-golambda.invoke_arn
}


############ Deployment of API Gateway ############

resource "aws_api_gateway_deployment" "deployment1" {
  rest_api_id = aws_api_gateway_rest_api.dev-golambda.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.dev-golambda.body))
  }

  depends_on = [aws_api_gateway_integration.integration]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "example" {
  deployment_id = aws_api_gateway_deployment.deployment1.id
  rest_api_id   = aws_api_gateway_rest_api.dev-golambda.id
  stage_name    = var.profile
}
