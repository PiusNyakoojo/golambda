// zip the code
data "archive_file" "zip" {
  type        = "zip"
  source_file = "../target/golambdabin"
  output_path = "../target/golambdabin.zip"
  depends_on  = [null_resource.compile]
}