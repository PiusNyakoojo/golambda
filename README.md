
# golambda

`golambda` is a deployment of a [Golang](https://go.dev/) serverless function to [AWS Lambda](https://aws.amazon.com/lambda/). This particular serverless function implements returning information about an IP Address using ________.

## About
**Serverless functions** are a single-purpose, programmatic feature of serverless computing -- also simply called "serverless" -- a cloud computing execution model where the cloud provider provisions computing resources on demand for its customers and manages all architectures, including cloud infrastructure. [1]


## Features
* 📚 **API Gateway**: https://o0hhesny6h.execute-api.us-east-1.amazonaws.com/dev-01/person?ipAddress=github.com


## Completed Tasks

- [x] Deploy using Terraform
- [x] Hide secret environment variables from Git Distribution
- [x] Create branches and merge branches instead of pushing directly to master
- [x] Support lambda query parameter input
- [x] Access lambda function through API Gateway
- [ ] Add IP Address Lookup Feature
- [ ] Add Unit Test for function
- [ ] Deploy Lambda function using Gitlab CI/CD
- [ ] Add gitlab badge for completed tests
- [ ] Add gitlab badge for test coverage
- [ ] Add gitlab badge for pipeline status

## Development Summary

I spent a lot of time working out the problem I was having with getting API Gateway to expose the lambda function by a REST API. I was receiving a 403 Forbidden error repeatedly in the API Gateway to Lambda function Test API calls. I figured that this problem was relating to how I was orchestrating the permissions for `aws_lambda_permission` and spent time looking at that.

## Development Log

(Day 2) [February 13, 2022 @ 18:52 - ]


(Day 2) [February 13, 2022 @ 04:05 - 05:00]:
- Started working on the development log.
- Fixed issue with API Gateway communicating with Lambda Function. Updated the execution role of the lambda proxy and deployed the resource api.
- Started researching GeoIP Lookup API Resources.


(Day 1) [February 12, 2022 @ 10:00 - 18:00]:
- Debugged problem with API Gateway user policy permission.
- Fixed problem with user permission for creating the API Gateway.
- Ran into problem with API Gateway sending messages to the Lambda function.
- Debugged problem with API Gateway communicating with the Lambda function. Received 403 Forbidden error.


(Day 1) [February 12, 2022 @ 00:00 - 06:00]:
- Started working on the project.
- Started reading the project description.
- Deployed "hello-world" lambda function to AWS using Terraform
- Published git repository to gitlab
- Ran into problem with publishing the API Gateway without appropriate user policy permissions



## Development Guide

### Invoking the Function using AWS
```bash
aws --profile dev-01 lambda invoke --function-name dev-golambda response.json
```

## Software Dependencies

| Tooling | Testing |
|--|--|
| [AWS Lambda Go](https://github.com/aws/aws-lambda-go) | |


## Learn More

🎬 Watch a video tutorial: [Video](https://www.youtube.com/watch?v=CtZS6Z54Hrs&ab_channel=LearnByExamples)


## References

[1]
[What Are Serverless Functions?](https://www.splunk.com/en_us/data-insider/what-are-serverless-functions.html#:~:text=Serverless%20functions%20are%20a%20single,all%20architectures%2C%20including%20cloud%20infrastructure.)
